package com.ituks.hack;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.TextView;

public class ServerActivity extends Activity {

    private ServerSocket serverSocket;

    Handler updateConversationHandler;

    Thread serverThread = null;

    private TextView text;

    public static final int SERVERPORT = 6000;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_server);

        text = (TextView) findViewById(R.id.text2);

        updateConversationHandler = new Handler();

        this.serverThread = new Thread(new ServerThread());
        this.serverThread.start();

    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public class ServerThread implements Runnable {

        public void run() {
            Socket socket = null;
            Log.e("INFO","Starting ServerThread");
            try {
                serverSocket = new ServerSocket(SERVERPORT);
            } catch (IOException e) {
                Log.e("DEBUG",e.getMessage());
            }
            while (!Thread.currentThread().isInterrupted()) {

                try {
                    socket = serverSocket.accept();

                    CommunicationThread commThread = new CommunicationThread(socket);
                    new Thread(commThread).start();
                    //Thread.currentThread().interrupt();

                } catch (IOException e) {
                    Log.e("DEBUG",e.getMessage());
                }
            }
            text.setText("Closed");
        }
    }

    class CommunicationThread implements Runnable {

        private Socket clientSocket;

        private BufferedReader input;

        public CommunicationThread(Socket clientSocket) {

            this.clientSocket = clientSocket;

            try {

                this.input = new BufferedReader(new InputStreamReader(this.clientSocket.getInputStream()));

            } catch (IOException e) {
                Log.e("DEBUG",e.getMessage());
            }
        }

        public void run() {

            while (!Thread.currentThread().isInterrupted()) {

                try {

                    String read = input.readLine();


                    updateConversationHandler.post(new HandleClient(read));

                    Thread.currentThread().interrupt();
                } catch (IOException e) {
                    Log.e("DEBUG",e.getMessage());
                }
            }
            //text.setText("Interrupted");

        }

    }

    class HandleClient implements Runnable {
        private String msg;

        public HandleClient(String str) {
            this.msg = str;
        }

        @Override
        public void run() {
            try {
                if (msg.equals("in")) {
                    text.setText("Input Connected");
                } else if (msg.equals("out")) {
                    text.setText("Output Connected");
                } else {
                    text.setText("Unknown");
                }
            } catch(Exception e) {
                Log.e("Debug",e.getMessage());
                text.setText("Interrupted");
            }
        }
    }
}